<%@page contentType = "text/html;charset = UTF-8" language = "java" %>
<%@page isELIgnored = "false" %>
<html>
   <head>
      <title>Spring MVC Form Handling</title>
      <style>
h2 {
text-align: center;
font-family: sans-serif;
}
form {
display: flex;
flex-direction: column;
width: 100%;
}
.form-input {
display: flex;
flex-direction: column;
width: 30%;
margin: 0px auto;
}
input {
max-width: 100%;
margin-bottom: 15px;
padding: 15px;
border: 1px solid #dddddd;
border-radius: 5px;
}
button {
padding: 15px;
cursor: pointer;
border: none;
border-radius: 5px;
background: linear-gradient(to right, blue, aqua);
font-size: 1em;
font-weight: bold;
color: white;
}
button:hover {
box-shadow: 0px 4px 10px blue;
}

td, th {
border: 1px solid #000000;
text-align: left;
padding: 8px;
}
tr:nth-child(even) {
background-color: #dddddd;
}
</style>
   </head>

   <body>
      <h2>Submitted Product Information</h2>
      <table>
      <tr>
         <td>Code</td>
            <td>${info.code}</td>
         </tr>
         <tr>
            <td>Name</td>
            <td>${info.name}</td>
         </tr>
         <tr>
            <td>Type</td>
            <td>${info.type}</td>
         </tr>
         <tr>
            <td>Price</td>
            <td>${info.price}</td>
         </tr>
      </table> 
      <a href = "/ProductWebSpring/product-list"> See Products</a> 
   </body>
   
</html>