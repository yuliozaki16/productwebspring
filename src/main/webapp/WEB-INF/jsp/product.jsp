<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Spring MVC Form Handling</title>
<style>
.error {
	color: red
}

h2 {
	text-align: center;
	font-family: sans-serif;
}

form {
	display: flex;
	flex-direction: column;
	width: 100%;
}

.form-input {
	display: flex;
	flex-direction: column;
	width: 30%;
	margin: 0px auto;
}

input {
	max-width: 100%;
	margin-bottom: 15px;
	padding: 15px;
	border: 1px solid #dddddd;
	border-radius: 5px;
}

button {
	padding: 15px;
	cursor: pointer;
	border: none;
	border-radius: 5px;
	background: linear-gradient(to right, blue, aqua);
	font-size: 1em;
	font-weight: bold;
	color: white;
}

button:hover {
	box-shadow: 0px 4px 10px blue;
}


</style>
</head>

<body>
	<h2>Product Input</h2>
	<form:form method="POST" action="/ProductWebSpring/addProduct">

		<table>
			<tr>
				<td><form:label path="code">Code</form:label></td>
				<td><form:input path="code" /></td>
			</tr>
			<tr>
				<td><form:label path="name">Name</form:label></td>
				<td><form:input path="name" /></td>
				<td><form:errors path="name" cssClass="error" /></td>
			</tr>
			<tr>
				<td><form:label path="type">Type</form:label></td>
				<td><form:input path="type" /></td>
				<td><form:errors path="type" cssClass="error" /></td>
			</tr>
			<tr>
				<td><form:label path="price">Price</form:label></td>
				<td><form:input path="price" /></td>
				<td><form:errors path="price" cssClass="error" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>

</html>