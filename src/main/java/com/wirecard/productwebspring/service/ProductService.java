package com.wirecard.productwebspring.service;

import java.util.List;

import com.wirecard.productwebspring.domain.Product;


public interface ProductService {

	void save(Product product);

	List<Product> findAll();

	Product findByCode(Integer code);

}