package com.wirecard.productwebspring.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wirecard.productwebspring.domain.Product;
import com.wirecard.productwebspring.repository.ProductRepository;
import com.wirecard.productwebspring.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	ProductRepository productRepo;
	
	@Override
	@Transactional
	public void save(Product product) {
		this.productRepo.save(product);
	}
	
	@Override
	@Transactional
	public List<Product> findAll(){
		return this.productRepo.findAll();
	}
	
	@Override
	@Transactional
	public Product findByCode(Integer code) {
		return productRepo.findByCode(code);
	}
}
