package com.wirecard.productwebspring.viewmodel;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ProductVM {
	@NotNull(message = "Product Code can't be empty")
	private Integer code;
	
	@NotEmpty(message = "Product Name can't be empty")
	private String name;
	
	@NotEmpty(message =  "Product Type can't be empty")
	private String type;
	
	@NotNull(message = "Product Price can't be null")
	private Double price;
	
	
	public ProductVM() {
		
	}


	public ProductVM(Integer code, String name, String type, Double price) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.price = price;
	}


	public Integer getCode() {
		return code;
	}


	public void setCode(Integer code) {
		this.code = code;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Double getPrice() {
		return price;
	}


	public void setPrice(Double price) {
		this.price = price;
	}
	
	
	
	
	
}
