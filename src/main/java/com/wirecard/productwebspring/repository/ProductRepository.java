package com.wirecard.productwebspring.repository;

import java.util.List;

import com.wirecard.productwebspring.domain.Product;

public interface ProductRepository {

	void save(Product product);

	List<Product> findAll();

	Product findByCode(Integer code);

}