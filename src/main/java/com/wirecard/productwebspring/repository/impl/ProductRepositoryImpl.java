package com.wirecard.productwebspring.repository.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wirecard.productwebspring.domain.Product;
import com.wirecard.productwebspring.repository.ProductRepository;

@Repository
public class ProductRepositoryImpl implements ProductRepository {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void save(Product product) {
		Session session = sessionFactory.getCurrentSession();
		session.save(product);
	}
	
	@Override
	public List<Product> findAll(){
		Session session = sessionFactory.getCurrentSession();
		List<Product> list = session.createQuery("from product_tbl").list();	
		return list;
	
	}
	
	@Override
	public Product findByCode(Integer code) {
		Session session = sessionFactory.getCurrentSession();
		Product product = session.load(Product.class, code);
		
		return product;
	}
}
