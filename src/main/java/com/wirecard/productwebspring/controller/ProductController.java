package com.wirecard.productwebspring.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.wirecard.productwebspring.domain.Product;
import com.wirecard.productwebspring.service.ProductService;
import com.wirecard.productwebspring.viewmodel.ProductVM;

@Controller
public class ProductController {
	
	@Autowired(required=true)
	ProductService productService;

	@GetMapping("/product")
	public ModelAndView product() {
		
		return new ModelAndView("product","command", new ProductVM());
	}
	
	@PostMapping("/addProduct")
	public String addProduct(@ModelAttribute("command") @Valid ProductVM productVm, BindingResult br, Model model) {
		//processing
		if(br.hasErrors()) {
			return "product";
		}else {
			Product product = new Product(productVm.getCode() , productVm.getName() , productVm.getType() , productVm.getPrice());
			productService.save(product);
			
			model.addAttribute("info", productVm);
			return "result";
		}
		
	}
}
